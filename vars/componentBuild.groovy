
def call(Map params = null, Closure body = null){
    node(){
        try{
            if(!body){
                build(params)
            } else{
                body()
            }
        } catch(err){
            def errorMessage = err.getMessage()
            echo 'Error:' + errorMessage
        }finally {
            cleanWs()

        }
    }
}


void build(Map params = null){
    def registry= "602182454152.dkr.ecr.us-east-1.amazonaws.com"
    def version = functions.getVersion()
    def tag = functions.getTag(version,registry)
    def newVersion

    stage("init"){    
        git branch: "${env.BRANCH_NAME}",
                credentialsId: 'gitlab',
                url: "git@gitlab.com:googlems/${functions.msName()}.git"        
    }
    stage("Build Image"){    
        sh "ls -l"           
        sh "docker build . -t ${tag}"           
    }
    stage("Login to Ecr"){
        withAWS(region:'us-east-1',credentials:'jenkins-user') {
            functions.awsLogin("docker",registry)
        }        
    }
    stage("Push Image to Ecr"){
        withAWS(region:'us-east-1',credentials:'jenkins-user') {
            sh "docker push ${tag}"
            // clean up docker image
            sh " docker image rm ${tag}"
        }
    }
    stage("package and push helm chart"){
        sh "ls -l"
        dir("${WORKSPACE}/helm"){
            // sh "ls -l"
            withEnv(['HELM_EXPERIMENTAL_OCI=1']){
                withAWS(region:'us-east-1',credentials:'jenkins-user'){
                    // read values.yaml and update deployment.image.tag with new build tag
                    def valuesYaml=  readYaml file: "values.yaml"
                    echo "values before:"
                    sh "cat values.yaml"
                    valuesYaml.deployment.image.tag = version
                    writeYaml file: 'values.yaml', data: valuesYaml, overwrite: true
                    echo "values after:"
                    echo "values after: "+ valuesYaml

                    // read the chart.yaml and update chart.version
                    def chartYaml = readYaml file: "Chart.yaml"
                    echo "values before:"
                    sh "cat Chart.yaml"
                    currentVersion = chartYaml.version
                    newVersion = "${currentVersion}-${version}"
                    chartYaml.version= newVersion
                    writeYaml file: 'Chart.yaml', data: chartYaml, overwrite: true
                    echo "values after:"
                    sh " cat Chart.yaml"

                    // package and push 
                    functions.awsLogin("helm registry",registry)
                    sh "rm -rf *tgz"
                    sh " helm package ."
                    sh"helm push *.tgz oci://${registry}"


                }
            }
        }
    }
    stage("Package and push global chart"){
        withAWS(region:'us-east-1',credentials:'jenkins-user') {
            dir("tempCheckout"){
                functions.chechoutGlobalChart()
                // iterate over global webstore chart and update the ms depenency version with new version
                functions.updateMsVersionInGlobalChart(newVersion, functions.msName() )
                functions.awsLogin("helm registry",registry)
                // push global chart to ecr
                functions.pushGlobalChart(registry)
                // commit changes to gitlab
                functions.commitChanges()
            }
        }
    }
}