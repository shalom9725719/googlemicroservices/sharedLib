
def call(Map params = null, Closure body = null){
        properties([
            parameters([
            string(name: 'namespace', description: ' namespace to deploy in'),
            ])
    ])
    node(){
        try{
            if(!body){
                build(params)
            } else{
                body()
            }
        } catch(err){
            def errorMessage = err.getMessage()
            echo 'Error:' + errorMessage
        }finally {
            cleanWs()

        }
    }
}
void build(Map params = null){
    stage("delete temp env"){
        withAWS(region:'us-east-1',credentials:'jenkins-user') {
            def registry= "602182454152.dkr.ecr.us-east-1.amazonaws.com"
            functions.awsLogin("helm registry",registry)
            try {
                sh " aws eks update-kubeconfig --name demo-cluster"
                sh " helm uninstall  ${env.namespace}  -n ${env.namespace}"
                sh 'curl -O "https://s3.us-west-2.amazonaws.com/amazon-eks/1.23.17/2023-03-17/bin/linux/amd64/kubectl"'  
                sh 'chmod u+x ./kubectl' 
                sh " ./kubectl delete ns ${env.namespace}"
            }catch(err){
                def errorMessage = err.getMessage()
                echo 'Error:' + errorMessage 
            }
        }
    }

}